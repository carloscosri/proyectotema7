<?php
require_once 'lib/View.php';

class LoginView extends View
{
    function __construct()
    {
        parent::__construct();
    }

    public function render($template='login.tpl')
    {  
        $this->smarty->display($template);
        $this->smarty->assign('userLogin', $_SESSION['user']);
    }

}
