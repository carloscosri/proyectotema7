{include file="template/header.tpl" title="encabezado"}
<div id="content">
    <h2>{$language->translate("order")}</h2>
    <table>
        <thead>
            <th>{$language->translate('code')}</th>
            <th>{$language->translate('name')}</th>
            <th>{$language->translate('price')}</th>
        </thead>
        <tbody>
            {foreach $rows as $row}
            <tr id="row{$row.id}">
                <td class="cell">{$row.codigo}</td>
                <td class="cell">{$row.nombre}</td>
                <td class="cell">{$row.precio}</td>
            </tr>
            {/foreach}
        </tbody>
        <button value='#'>Finalizar Compra</button>
        <tfoot>
        </tfoot>
    </table>
 </div>
{include file="template/footer.tpl" title="footer"}

<!--
{include file="template/header.tpl" title="encabezado"}
<div id="content">
    <br>
     <h2>PedidosRealizados</h2>
    <table>
        <thead>
            <th>Codigo de pedido</th>
            <th>Fecha de realizacion</th>
            <th>Estado del servicio</th>                               
            {if $idRole ge 3}
                <th>Usuario</th>
                {/if}
        </thead>
        <tbody>
        {foreach $pedidosHechos as $row}
            <tr ide="{$row['id']}" class="listaPedidosRealizados">
                <td>{$row['id']}</td>
                <td>{$row['fechaPedido']}</td>
                <td>{if $row['estado'] eq 0}Pedido{else}Realizado{/if}</td>
               {if $idRole ge 3}
                   <td>{$row['idUsuario']}</td>
                   {/if}
            </tr>          
        {/foreach}
        </tbody>      
    </table>
    <h2>Lista de pedido</h2>
    <table>
        <tr>           
            <th>Nombre</th>
            <th>Precio</th>
            <th>Unidades</th>
            <th>Total</th>           
            <th>Operaciones</th>
        </tr>
        <tbody>
        {foreach $listaPedido as $row}
            <tr id="ped{$row['id']}" class="listadoPedidos">

                <td>{$row['nombre']}</td><td>{$row['precio']} €</td><td><input type="number" name="quantity" class="cantidad" precio="{$row['precio']}" ide="{$row['id']}" min="1" max="200" value="{$row['cantidad']}"></td><td id="total{{$row['id']}}">{$row['cantidad']*$row['precio']} €</td><td><a href="#" class="eliminarPedido" ide="{$row['id']}">Eliminar</a></td>

            </tr>
            {$totalPrecio = $totalPrecio + ($row['precio']*$row['cantidad'])}
        {/foreach}
        </tbody>
        <tfoot>
            <tr><td>Total</td><td>{$totalPrecio} €</td><td></td><td></td><td><a href="#" id="botonPedido">REALIZAR PEDIDO</a></td></tr>
        </tfoot>
    </table>
</div>
{include file="template/footer.tpl" title="footer"}
-->