{include file="template/header.tpl" title="encabezado"}
<div id="content">
    <br>
    <h2>{$language->translate('register')}</h2>
    
    <form action="{$url}{$lang}/user/insert" method="post">
        <label>Nombre</label><input type="text" name="name"><br>
        <label>Password</label><input type="password" name="password"><br>
        {if ($rol == 1)}
            <input type="text" name="idRole" value=2 style="display: none">
        {/if}
        {if ($rol != 1)}
            <label>Role</label>
            <select name="idRole" >
                <option selected value="0">
                {foreach $roles as $role}
                <option value="{$role.id}">
                    {$role.role} 
                </option>
                {/foreach}
            </select> 
        {/if}
        <br>
        <label></label><input type="submit" value="Enviar">
    </form>

 </div>
{include file="template/footer.tpl" title="footer"}