<!DOCTYPE html>

<html>
    <head>
        <title>TODO: supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="{$url}public/css/default.css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <script src="{$url}public/js/jquery.js" type="text/javascript"></script>
        {if isset($js) && count($js)}
        {foreach item=file from=$js}        
        <script src="{$url}public/js/{$file}" type="text/javascript"></script>        
        {/foreach}   
        {/if}
         
    </head>
    <body>
        <div id="header">
            <div id="title">
                TIENDA
            </div>
            <div  style="float: left">
               {* <a href="{$url}{$lang}/index" >{$language->translate('index')}</a>*}                
                {if ($rol == 1)}
                <a href="{$url}{$lang}/user/add">{$language->translate('register')}</a>
                {/if}                
                {if ($rol == 3)}
                <a href="{$url}{$lang}/user" >{$language->translate('user')}</a>
                {/if}
                <a href="{$url}{$lang}/{$log}" >{$log}</a>              
                <a href="{$url}{$lang}/product" >{$language->translate('product')}</a>
                {if ($rol == 2)}
                <a href="{$url}{$lang}/product/order" >{$language->translate('order')}</a>                
                {/if}
                {if ($rol == 3)}
                <a href="#" >TOTAL_PEDIDOS</a>                
                {/if}                
            </div>
            <div  style="float: left ; padding-left:4em">
                <a href="{$url}es/index" class="fa fa-eur"></a> 
                <a href="{$url}en/index" class="fa fa-gbp"></a>
            </div>
            
            <div id="uuario">
                {$language->translate('user')}: {$usr}
            </div>
        </div>