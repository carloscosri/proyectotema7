$(document).ready(function (){
    var loadDoc = function (numero) {
        $(document).attr("pagina", numero);
        var url = 'http://localhost/proyectotema7/es/product/ajaxPageData/' + numero;
        $.get(url, '', function(data) {
            $("#tbodyList").empty();
            for(var i = 0; i<data.length; i++){
                newRows = "";
                newRows = '<tr id="row'+data[i].id+'"><td class="cell">' + data[i].id + '</td>';
                newRows = newRows + '<td class="cell">' + data[i].codigo + '</td>';
                newRows = newRows + '<td class="cell">' + data[i].nombre + '</td>';
                newRows = newRows + '<td class="cell">' + data[i].precio + '</td>';
                newRows = newRows + '<td class="cell">' + data[i].existencia + '</td>';
                newRows = newRows + '<td><button class="buy" value='+data[i].id+'\
                co="' + data[i].codigo + '"\n\
                no="' + data[i].nombre + '"\n\
                pr="' + data[i].precio + '"\n\
                >comprar</button>';
                newRows = newRows + '<button class="del" value='+data[i].id+'>borrar</button></td></tr>';
               $("#tbodyList").append(newRows);
            }
        }, 'json' );
    };

    var numeroBotones = function () {
        var url = 'http://localhost/proyectotema7/es/product/ajaxNumPages/';
        $.get(url, '', function(data) {
            $("#buttons").empty();
            var calcButtons = data;
            var newButtons = "";
            for(var i = 1; i<=calcButtons; i++){
                newButtons = '<button class="push" value='+i+'>'+i+'</button>';
                $("#buttons").append(newButtons);
            }
        } );
    };

    $(document).on('click','.push',(function () {
        var num = $(this).val();
        loadDoc(num);
    }));

    numeroBotones();
    loadDoc(1);

    $(document).on('click','.del',(function () {
        var num = $(this).val();
        ajaxDelete(num);
    }));

    var ajaxDelete = function (numero) {
        var url = 'http://localhost/proyectotema7/es/product/ajaxDelete/'+ numero;
        $.get(url, '', function(data) {
            pagina = $(document).attr("pagina");
            loadDoc(pagina);
            numeroBotones();
            if(data){
                $("#row" + numero).empty();
            }
            else{
                alert("Borrado Fallido");
            }
        }, 'json' );
    };

    $(document).on('click','.ins',(function () {
        var codigo = $('#codigo').val();
        var nombre = $('#nombre').val();
        var precio = $('#precio').val();
        var existencia = $('#existencia').val();
        ajaxInsert(codigo, nombre, precio, existencia);
    }));

        var limpiarFormulario = function (){
            $('#codigo').val("");
            $('#nombre').val("");
            $('#precio').val("");
            $('#existencia').val("");
        };

        var ajaxInsert = function (codigo, nombre, precio, existencia) {
        var url = 'http://localhost/proyectotema7/es/product/ajaxInsert/';
        $.post(url, 
            {
                codigo: codigo,
                nombre: nombre,
                precio: precio,
                existencia: existencia
            }
            , function(data) {
            pagina = $(document).attr("pagina");
            loadDoc(pagina);
            numeroBotones();
            if(data){
                limpiarFormulario();
            }
            else{
                alert("Inserción Fallida");
            }
        }, 'json' );
    };  
    
    $(document).on('click','.buy',(function () {
        var codigo = $(this).attr('co');
        var nombre = $(this).attr('no');
        var precio = $(this).attr('pr');
        ajaxBuy(codigo, nombre, precio);
       
    }));
    
    var ajaxBuy = function (codigo, nombre, precio) {
        var url = 'http://localhost/proyectotema7/es/product/ajaxAddOrderList';
        $.post(url, 
            {
                codigo: codigo,
                nombre: nombre,
                precio: precio
            }
            , function() {
        }, 'json' );
    };
    
    $("#filter").keyup(function () {
        filterTable($(this).val());
    });
    
    var filterTable = function (text) {
        url = 'http://localhost/proyectotema7/es/product/ajaxGetDataFilter/';
        $.get(url, {filter: text}, function (data) {
            rows = data;
            $("#tbodyList").empty();
            for (var i = 0; i < rows.length; i++) {

                newRows = '<tr class="cell" id="row' + rows[i].id + '" idRow=' + rows[i].id + ' >';
                newRows = newRows + '<td > ' + rows[i].id + '</td>';
                newRows = newRows + '<td > ' + rows[i].codigo + '</td>';
                newRows = newRows + '<td > ' + rows[i].nombre + '</td>';
                newRows = newRows + '<td > ' + rows[i].precio + '</td>';
                newRows = newRows + '<td > ' + rows[i].existencia + '</td>';
                newRows = newRows + '<td><button class="buy" value='+data[i].id+'\
                co="' + data[i].codigo + '"\n\
                no="' + data[i].nombre + '"\n\
                pr="' + data[i].precio + '"\n\
                >comprar</button>';
                newRows = newRows + '<button class="del" value='+data[i].id+'>borrar</button></td></tr>';
                $("#tbodyList").append(newRows);
            }
            $(document).on('click', '.deleteRecord', function () {
                idRow = this.id;
                idRow = idRow.substring(6);
                deleteRecord(idRow);
            });
        }, 'json');       
    };
    
    /*    
    $(document).on("click", '.pedido', function (e) {
        e.preventDefault();
        var idProd = $(this).attr("value");
        var precio = $(this).attr("pr");
        var nombre = $(this).attr("no");
        nuevoPedido(idProd, precio, nombre);
    });
    
    var nuevoPedido = function (idProd, precioProd, nombreProd) {
        $.post('http://localhost/proyectotema7/es/product/ajaxNuevoPedido',
                {id: idProd, nombre: nombreProd, precio: precioProd, cantidad: "1"}, function (data) {
            alert("Producto añadido al carro");
        }, 'json');
    };
    
    var seleccionarPedidoRealizado = function (idPed) {
        $.post('http://localhost/examen2alb/es/order/searchDetailsOrder',
                {id: idPed}, function (data) {
            var texto = "";
            var precioTotal = 0;

            for (var i = 0; i < data.length; i++) {
                texto += "Linea: " + data[i].linea + " Codigo Producto: " + data[i].idProducto + " Unidades: " + data[i].cantidad + " Precio/u: " + data[i].precio + " \n";
                precioTotal = parseFloat(precioTotal + (data[i].cantidad * data[i].precio));
            }
            texto += "Precio total de la compra: " + precioTotal + " €";
            alert(texto);
        }, 'json');
    };

    var addPedido = function () {
        $.post('http://localhost/examen2alb/es/order/ajaxAddOrder', "", function (data) {
            if (data) {
                $(".listadoPedidos").empty();
                alert("Pedido realizado con exito! \n Para ver el estado de su pedido, pulse arriba");
            }
        }, 'json');

    };
    var deletePedido = function (idProd) {
        $.post('http://localhost/examen2alb/es/order/ajaxDeleteOrder', {id: idProd}, function (data) {
            if (data) {
                $("#ped" + idProd).empty();
            } else {
                alert("Borrado no realizado con exito");
            }
        }, 'json');
    };
    */
});