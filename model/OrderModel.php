<?php

require_once 'lib/Model.php';

class OrderModel extends Model {

    function __construct() {

        parent::__construct();
    }

    protected function delete($numero) {
        
    }

    protected function get($numero) {
        
    }

    protected function getAll() {
        
    }

    protected function insert($fila) {
        
    }

    protected function update($fila) {
        
    }

    public function addOrder() {

        $idUser = $_SESSION['idUsuario'];

        $this->_sql = "INSERT INTO pedido(fechaServido, estado, idUsuario) "
                . "VALUES (0, 0,'" . $idUser . "')";

        if ($this->executeQuery()) {

            return $this->insertId();
        } else {
            return false;
        }
    }
    
    public function showOrdersUser() {
        $idUser = $_SESSION['idUsuario'];
        
        if($_SESSION['accessLevel']>=3){
            $this->_sql = "SELECT * FROM pedido";
        }else{
        
        
        $this->_sql = "SELECT * FROM pedido WHERE idUsuario ='$idUser'";
        }
        $this->executeSelect();
        return $this->_rows;
    }
    public function selectDetailsOrder($idPedido) {
        
        $this->_sql = "SELECT * FROM detallepedido WHERE idPedido ='$idPedido'";
        $this->executeSelect();
        return $this->_rows;
    }

    public function addOrderProducts($idPedido) {

        $arrayProd = $_SESSION['listaPedido'];
        $idNueva = 0;
        $contador = 0;
        foreach ($arrayProd as $producto) {
            $contador++;
       
         $idP = $producto['id'];
         $precioP = $producto['precio'];
         $cantidadP = $producto['cantidad'];
            
           $this->_sql = "INSERT INTO detallepedido (idPedido, linea, idProducto, cantidad, precio) VALUES ('" . $idPedido."','". $contador ."', '".$idP."', '".$cantidadP."', '".$precioP."')";

           $this->executeQuery();
        }
        unset($_SESSION['listaPedido']);
        return true;
    }

}
